using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV6
{
    class Program
    {
        static void Main(string[] args)
        {
            Notebook notebook1 = new Notebook();
            notebook1.AddNote(new Note("Petar Pavic", "Pozega"));
            notebook1.AddNote(new Note("Luka Modric", "Madrid"));
            notebook1.AddNote(new Note("Borna Coric", "Zagreb"));
            Iterator iterator = new Iterator(notebook1);
            iterator.Current.Show();
            iterator.Next().Show();
            while (iterator.IsDone != true)
            {
                iterator.Current.Show();
                iterator.Next();
            }
        }
    }
}